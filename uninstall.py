import os


def checkroot():
    euid = os.geteuid()
    if (os.geteuid() == 0):
        return True
    return False


if not checkroot():
    print("Script must be run as root")
    exit()

print("Uninstalling...")
if (os.path.exists("/opt/waterfox/")):
    os.system("rm -rf /opt/waterfox/")

if (os.path.exists("/bin/waterfox")):
    os.system("rm -f /bin/waterfox")

if (os.path.exists("/usr/share/applications/waterfox.desktop")):
    os.system("rm -f /usr/share/applications/waterfox.desktop")