# Waterfox Installer
A python install script for waterfox on linux

# DO NOT USE!
After a large waterfox update, this program no longer installs waterfox properly and leaves a bunch of useless files all over your system.

## Dependencies
1. Requests python package (for getting latest waterfox version availible)
2. Curl

## Installing (No cloning!)
Run this command:  
```
curl -s "https://codeberg.org/Garbg/waterfox-install/raw/branch/main/install.py" -o waterfox-install.py && sudo python3 waterfox-install.py
```
## Uninstalling (No cloning!)
Run this command:
```
curl -s "https://codeberg.org/Garbg/waterfox-install/raw/branch/main/uninstall.py" -o waterfox-uninstall.py && sudo python3 waterfox-uninstall.py
```
## Updating
***Same as installing!***
### How to install requests
Run this command: `pip install requests`
