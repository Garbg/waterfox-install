import requests
import os

def checkroot():
    euid = os.geteuid()
    if (os.geteuid() == 0):
        return True
    return False


if not checkroot():
    print("Script must be run as root")
    exit()

get = requests.get("https://github.com/WaterfoxCo/Waterfox/releases/latest")
releasenum = str(get.url).replace("https://github.com/WaterfoxCo/Waterfox/releases/tag/", "")
print("Downloading...")
os.system(f'curl -s "https://cdn1.waterfox.net/waterfox/releases/{releasenum}/Linux_x86_64/waterfox-{releasenum}.tar.bz2" -o waterfox.tar.bz2')
print("Latest release of waterfox downloaded!\nExtracting...")
os.system("tar xjf waterfox.tar.bz2")
print("Extracted!")
os.system("rm -f waterfox.tar.bz2")
print("Downloading desktop file...")
os.system('curl -s "https://codeberg.org/Garbg/waterfox-install/raw/branch/main/waterfox.desktop" -o waterfox.desktop')
print("Desktop file downloaded!\nInstalling...")
if (os.path.exists("/opt/waterfox/")):
    os.system("rm -rf /opt/waterfox/")
os.system("cp -r waterfox/ /opt/waterfox")
os.system("rm -rf waterfox/")
if (os.path.exists("/bin/waterfox")):
    os.system("rm -f /bin/waterfox")
os.system("ln -s /opt/waterfox/waterfox /bin/waterfox")
if (os.path.exists("/usr/share/applications/waterfox.desktop")):
    os.system("rm -f /usr/share/applications/waterfox.desktop")
os.system("mv waterfox.desktop /usr/share/applications/")
print("Installation done!")
